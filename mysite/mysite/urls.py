from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('cf/', include('cf.urls')),
    path('admin/', admin.site.urls),
]
