from django.urls import path

from . import views

app_name= 'cf'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:persona_id>', views.detail, name='detail'),
    path('getcf/', views.getcf, name='getcf'),
  
]