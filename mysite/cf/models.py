from enum import unique
from pickle import TRUE
from unittest.util import _MAX_LENGTH
from django.db import models

class Persone(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    birthplace = models.CharField(max_length=100)
    sex = models.CharField(max_length=1)
    cod_fiscale = models.CharField(max_length=16, unique = TRUE)
    birthdate = models.DateField(auto_now=False,auto_now_add=False)
    def __str__(self):
        return self.name
         

# Create your models here.
